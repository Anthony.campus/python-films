import json
import datetime


def read_file(filename: str) -> list:
    """Lis un fichier et le retourne en JSON"""
    try:
        with open(filename, 'r', encoding='utf8') as json_file:
            data = json.load(json_file)
            return data
    except Exception as e:
        raise e


def filter_json(data: dict, key: str, value: str) -> dict:
    """Retourne toutes les valeurs d'un JSON qui correspondent au filtre key/value"""
    result = []
    for d in data:
        if value in d[key]:
            result.append(d)
    return list(result)


def remove_year_decorator(year: str) -> int:
    args = year.split("–")

    if len(args) == 2:
        if len(args[1]) != 0:
            return int(args[1])

    return int(args[0])


def date_is_after(date_string: str, max_date_string: str) -> bool:
    try:
        max_date_object = datetime.datetime.strptime(
            max_date_string, "%d/%m/%Y")
        date_object = datetime.datetime.strptime(date_string, "%d %b %Y")

        if max_date_object <= date_object:
            return True

        return False
    except Exception as e:
        print(e)
