# Your code goes here.
import utils


class File_manager:
    def __init__(self, file):
        self.file_data = utils.read_file(file)

    def count(self) -> int:
        """Compter le nombre d'éléments présents dans le fichier JSON"""
        return len(self.file_data)

    def written_by(self, author: str) -> dict:
        """Récupérer tous les éléments avec en argument un auteur"""
        return utils.filter_json(self.file_data, "Writer", author)

    def longest_title(self) -> dict:
        """Récupérer le film ayant le titre le plus long"""
        current_biggest = None

        for data in self.file_data:
            if current_biggest is None:
                current_biggest = data
            elif len(data["Title"]) > len(current_biggest['Title']):
                current_biggest = data

        return current_biggest

    def best_rating(self) -> dict:
        """Récupérer le film ayant la meilleure moyenne"""
        current_biggest = None

        for data in self.file_data:
            try:
                if current_biggest is None:
                    current_biggest = data
                elif float(data["imdbRating"]) > float(current_biggest["imdbRating"]):
                    current_biggest = data
            except Exception as e:
                print(e)

        return current_biggest

    def latest_film(self) -> dict:
        """Récupérer le film le plus récent"""
        most_last = None

        for data in self.file_data:
            try:
                if most_last is None:
                    most_last = data
                elif utils.remove_year_decorator(data["Year"]) >= utils.remove_year_decorator(most_last["Year"]):
                    most_last = data
            except Exception as e:
                print(e)

        return most_last['Title']

    def find_per_genre(self, moovie_type: str) -> list:
        """Récupérer tous les films du même genre"""
        return utils.filter_json(self.file_data, "Genre", moovie_type)

    def released_after(self, date: str) -> list:
        """Récupérer tous les films réalisés après la date mentionnée"""
        result = []

        for data in self.file_data:
            try:
                if utils.date_is_after(data["Released"], date):
                    result.append(data)
            except Exception as e:
                print(e)

        return result
